/*
 * Copyright (c) 1997 ... 2025 2026
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software
 * for any purpose with or without fee is hereby granted,
 * provided that the above copyright notice and this permission
 * notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 * jadd.c - main routines
 */

#include <sys/param.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#ifdef OpenBSD
#include <err.h>
#endif
#ifdef __FreeBSD_version
#include <err.h>
#endif
#ifdef __NetBSD_Version__
#include <err.h>
#endif

#ifdef HAVE_JLIB
#include <j_lib2.h>
#include <j_lib2m.h>
#endif

#include "jadd.h"

/*
* show_file_heading() -- Show run stats
*/
void show_file_heading(FILE *fp, char *fname, int subh, char *stdfmt, char *std_fname)

{

  fprintf(fp, "%s\n", LIT_C80);
  print_fname(fp, fname, stdfmt, std_fname);
  fprintf(fp, "\n%s\n", LIT_C80);

  if (subh == TRUE)
    {
      fprintf(fp,"\n");
      fprintf(fp,"      %15s  %28s  %s\n",
              LIT_RECORD, LIT_VALUE, LIT_ERRORS);
      fprintf(fp,"      %15s  %28s  %s\n",
              LIT_ULINE_15, LIT_ULINE_28, LIT_ULINE_25);
    }
} /* show_file_heading() */

/*
* print_vals()
*/
void print_vals(FILE *fp,
		struct s_values *a,
		int verbose,
		int show_file_count,
		char *title)
{

  if (verbose < 1)
    {
      fprintf(fp, "%.3Lf\n", a->total);
      return;
    }

  if ((verbose > 2) && (show_file_count == TRUE))
    show_file_heading(fp, LIT_TOTAL_ALL_FILES, FALSE, LIT_STDIN, FILE_NAME_STDIN);

  fprintf(fp, "\n");
  fprintf(fp, "%-15s %-5s: %28.3Lf\n", LIT_TOTAL,   title, a->total);
  fprintf(fp, "%-15s %-5s: %28.3Lf\n", LIT_AVERAGE, title, a->average);
  fprintf(fp, "%-15s %-5s: %28.3Lf\n", LIT_MINIMUM, title, a->min);
  fprintf(fp, "%-15s %-5s: %28.3Lf\n", LIT_MAXIMUM, title, a->max);
  fprintf(fp, "%-15s %-5s: %24ld\n",   LIT_READS,   title, a->reads);

  if (show_file_count == TRUE)
    fprintf(fp, "%-21s: %24ld\n", LIT_COUNT_FILES, a->file_count);

  fprintf(fp, "\n");

} /* print_vals() */

/*
* set_rec() -- set were the number should exist
*/
char *set_rec(struct s_work *w, char *buf, char **expanded)
{
  size_t len = 0;
  size_t size_expand = 0;
  size_t i = 0;
  char *buf_use = (char *) NULL;

  if ((*expanded) != (char *) NULL)
    {
      free((*expanded));
      (*expanded) = (char *) NULL;
    }

  if (w->col < 1)
    return(buf);

  if (w->delm == JLIB2_CHAR_NULL)
    {
      if (j2_expand_tab(8, &size_expand, expanded, buf) == FALSE)
	return((char *) NULL);
      buf_use = *expanded;
    }
  else
    buf_use = buf;

  len = strlen(buf_use);

  if (w->col < 1)
    return(buf_use);

  /*** processing a flat fixed length file ? ***/
  if (w->delm == JLIB2_CHAR_NULL)
    {
      if (len < 1)
	return((char *) NULL);
      if (w->col >= len)
	return((char *) NULL);
      return(&(buf_use[w->col]));
    }

  /*** record is delimited ***/
  if (j2_fix_delm(w->delm, expanded, buf_use) < 1)
    return((char *) NULL);
  buf_use = *expanded;

  if (w->col > 0)
    {
      for (i = 0, len = 0; buf_use[i] != JLIB2_CHAR_NULL; i++)
	{
	  if (buf_use[i] == w->delm)
	    len++;
	  if (len == w->col)
	    {
	      i++;
	      buf_use = &buf_use[i];
	      break;
	    }
	}
    }

  return(buf_use);

} /* set_rec() */

/*
* convert_numb()
*/
int convert_numb(struct s_work *w, struct s_values *vals, char *buf)
{
  size_t i = 0;
  char *p = (char *) NULL;

  j2_justleft(buf);

  for (i = 0; buf[i] != JLIB2_CHAR_NULL; i++)
    {
      if (buf[i] == '-')
	continue;
      if (! isdigit((int)buf[i]))
	{
	  if (isspace((int)buf[i]))
	    {
	      buf[i] = JLIB2_CHAR_NULL;
	      break;
	    }
	  if ((buf[i] == '.') || (buf[i] == ','))
	    continue;
	  buf[i] = JLIB2_CHAR_NULL;
	  break;
	}
    }

  if (j2_fix_numr(buf, w->thousands, w->decimal) == FALSE)
    {
      vals->current_value = (NUMBER) 0;
      fprintf(w->err.fp, MSG_WARN_W033L, vals->reads, w->col + 1);
      return(FALSE);
    }

  if (w->decimal == ',')
    {
      p = strstr(buf, ",");
      if (p != (char *) NULL)
	*p = '.';
    }

  vals->current_value = (NUMBER) strtold(buf, NULL);
  if (vals->reads > 1)
    {
      if (vals->min > vals->current_value)
	vals->min = vals->current_value;
      if (vals->max < vals->current_value)
	vals->max = vals->current_value;
    }
  else
    {
      vals->min   = vals->current_value;
      vals->max   = vals->current_value;
    }

  return(TRUE);

} /* convert_numb() */

/*
* process_a_file()
*/
void process_a_file(struct s_work *w,
                    char *fname,
                    char **buf,
                    size_t *bsize,
                    struct s_values *vals,
                    struct s_values *totals)

{

  FILE *fp;
  char datetime[14];
  char *pos = (char *) NULL;
  char *expanded = (char *) NULL;
  int numb_good = FALSE;

  if ( ! open_in(&fp, fname, w->err.fp) )
    return;

  if (w->verbose > 1)
    {
      if (w->verbose > 2)
	show_file_heading(w->out.fp, fname, TRUE, LIT_STDIN, FILE_NAME_STDIN);
      j2_d_fmtdt(datetime, 14);
      fprintf(w->err.fp, "%s - %s ", LIT_PROCESSING, 
             (j2_d_fmtdt(datetime, 14) == (char *) NULL ? "" : datetime));
      print_fname(w->err.fp, fname, LIT_STDIN, FILE_NAME_STDIN);
      fprintf(w->err.fp, "\n");
    }

  totals->file_count++;
  init_vals(vals);

  /*** process data ***/
  while (j2_getline(buf, bsize, fp) > (ssize_t) -1)
    {
      vals->reads++;
      j2_rtw((*buf));
      pos = set_rec(w, (*buf), &expanded);
      if (pos == (char *) NULL)
	{
	  if (w->verbose > 2)
	    fprintf(w->out.fp, "      %15ld  %28.3Lf  %s\n", vals->reads, (NUMBER) 0, LIT_INVALID_RECORD);
	  continue;
	}
      numb_good = convert_numb(w, vals, pos);
      vals->total +=vals->current_value;
      if (w->verbose > 2)
	{
	  if (numb_good == TRUE)
	    fprintf(w->out.fp, "      %15ld  %28.3Lf\n",     vals->reads, vals->current_value);
	  else
	    fprintf(w->out.fp, "      %15ld  %28.3Lf  %s\n", vals->reads, vals->current_value, LIT_INVALID_NUMERIC);
	}
    }

  if (vals->reads > 0)
    vals->average = vals->total / vals->reads;

  totals->writes    += vals->writes;
  totals->reads     += vals->reads;
  totals->total     += vals->total;
  if (totals->file_count > 1)
    {
      if (totals->min > vals->min)
	totals->min = vals->min;
      if (totals->max < vals->max)
	totals->max = vals->max;
    }
  else
    {
      totals->min = vals->min;
      totals->max = vals->max;
    }

  print_vals(w->out.fp, vals, w->verbose, FALSE, "");

  if (expanded != (char *) NULL)
    free(expanded);

  /*** complete ***/
  close_in(&fp, fname);


} /* process_a_file() */

/*
 * process_all() -- Process all files
 */
void process_all(int argc, char **argv, struct s_work *w)

{
  int i;
  char *buf = (char *) NULL;
  size_t bsiz = (size_t) 200;
  struct s_values totals;
  struct s_values vals;

  init_vals(&totals);
  init_vals(&vals);

  /* allocate initial read buffer memory (optional) */
  buf = (char *) calloc(bsiz, sizeof(char));
  if (buf == (char *) NULL)
    {
      fprintf(w->err.fp, MSG_ERR_E080, strerror(errno));
      return;
    }

  /* process files */
  for (i = optind; i < argc; i++)
    process_a_file(w, argv[i], &buf, &bsiz, &vals, &totals);

  if (i == optind)
    process_a_file(w, FILE_NAME_STDIN, &buf, &bsiz, &vals, &totals);

  if (totals.reads > (NUMBER) 0)
    totals.average   = totals.total / totals.reads;

  if (totals.file_count > 1)
    print_vals(w->out.fp, &totals, w->verbose, TRUE, LIT_ALL);

#ifdef OpenBSD
  freezero(buf, bsiz);
#else
  if (buf != (char *) NULL)
    free(buf);
#endif

}  /* process_all() */

/*
 * main()
 */
int main(int argc, char **argv)

{
  time_t tstart = clock();
  struct s_work w;

#ifdef OpenBSD
  if(pledge("stdio rpath wpath cpath",NULL) == -1)
    err(1, MSG_ERR_E100, 1, strerror(errno));
#endif

  init(argc, argv, &w);

  process_all(argc, argv, &w);

  if (w.verbose > 1)
    {
      fprintf(w.err.fp, MSG_INFO_I152S,
         (double)(clock() - tstart) * 1000 / (double) CLOCKS_PER_SEC);
    }

  close_out(&(w.out));
  close_out(&(w.err));
  if (w.prog_name != (char *) NULL)
    free(w.prog_name);
  exit(EXIT_SUCCESS);

}  /* main() */
