.\"
.\" Copyright (c) 2022 ... 2024 2025
.\"     John McCue
.\"
.\" Permission to use, copy, modify, and distribute this software
.\" for any purpose with or without fee is hereby granted,
.\" provided that the above copyright notice and this permission
.\" notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
.\" WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
.\" WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
.\" THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
.\" CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
.\" FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
.\" SOFTWARE.
.\"
.TH ALIST 1 "1997-11-17" "JMC" "User Commands"
.SH NAME
alist - Reads a list of Numbers and adds the up
.SH SYNOPSIS
templ [OPTIONS] [FILE...]
.SH DESCRIPTION
Will read a list if Numbers and add the up,
generating sum, average, max and minimum.
.PP
If no files (FILE) are specified on the command line or
if FILE has name '-', stdin (Standard Input) is used.
.TP
-C n
Optional, Column or Record Position were the
Number to added is on the record.
If '-d' is not specified, 'n' is were
on each record the Numeric Value is.
Default, process from the start of each record.
.TP
-D c
Optional, what is the decimal point of the Numbers
in the data being processed ?
Default, if not supplied a period (.) is used for the Decimal
Point.
.TP
-d delm
Optional, the input file is a delimited
file and the column to process is equal to 'delm'.
When this is specified, '-C' above is also needed.
Default, the file is a flat fixed length file.
.IP
You can supply the delimiter as a specific character or a base 10
integer that represents the character.
For example to use:
.nf
    delm  Use arg
    ----  -------------------
    |     -d '|'  OR -d 124
    TAB   -d 9
    ^G    -d 7
.fi
.TP
-e file
Optional, if used, write error messages to file 'file'.
If not specified, errors written to stderr.
.TP
-f
Optional, Force file create.
Create file even if the target file exists.
.TP
-h
Show brief help and exit.
.TP
-o file
Optional, if used, write output to file 'file'.
If not specified, output written to stdout.
.TP
-V
Output version information and exit.
.TP
-v
Optional, Verbose Level.  Print information about the run,
default do not show run messages.
Can be specified multiple times, each specification increases
verbose level:
.nf
    Level  Meaning
    -----  --------------------------------------------
    = 0    Print just the total on stdout and any
           Errors/Warnings on stderr.
    >= 1   Above plus print the Average, Maximum and
           Minimum Number found.
    >= 2   Above and show file processing statistics
           along with all Command Line parameters
           on stderr.
    >= 3   Above and all data used for the calculations
           along with rec # and headings (stdout)

.fi
.SH DIAGNOSTICS
When using -C without a Delimiter (-d), tabs will
be expanded in a similar manner as expand(1).
.PP
Output Format of Numbers depends upon your locale(1) settings.
.SH SEE-ALSO
awk(1),
bc(1),
cut(1),
dc(1),
expand(1),
jcsv(1),
jr(1),
locale(1),
paste(1),
sed(1)
.SH ERROR-CODES
.nf
0 success
1 processing error or help/rev displayed
.fi
