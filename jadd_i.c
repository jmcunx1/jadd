/*
 * Copyright (c) 2023 ... 2025 2026
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software
 * for any purpose with or without fee is hereby granted,
 * provided that the above copyright notice and this permission
 * notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 * jadd_i.c -- Initialization Routines
 */
#include <sys/param.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#ifdef HAVE_JLIB
#include <j_lib2.h>
#include <j_lib2m.h>
#endif

#include "jadd.h"

#define SCKARG 80

/*** prototypes ***/
void save_fname(struct s_file_info *, char *, char *, char);
char get_delm(FILE *, char *, char *);

/*
 * init_vals() -- initialize values
 */
void init_vals(struct s_values *v)
{
  v->file_count     = (size_t) 0;
  v->reads          = (size_t) 0;
  v->writes         = (size_t) 0;
  v->current_value  = (NUMBER) 0;
  v->min            = (NUMBER) 0;
  v->max            = (NUMBER) 0;
  v->average        = (NUMBER) 0;
  v->total          = (NUMBER) 0;
  v->raw_value      = (char *) NULL;

} /* init_vals() */

/*
 * init_fi() -- initialize File Information
 */
void init_fi(struct s_file_info *f)
{
  f->fp     = (FILE *) NULL;
  f->fname  = (char *) NULL;
} /* init_fi() */


/*
 * init_w() -- initialize work area
 */
void init_w(struct s_work *w, char *a)

{
  init_fi(&(w->out));
  init_fi(&(w->err));

  w->err.fp    = stderr;
  w->out.fp    = stdout;

  w->prog_name        = j2_get_prgname(a, PROG_NAME);
  w->num_files        = 0;
  w->verbose          = 0;
  w->force            = (int) FALSE;
  w->decimal          = DECIMAL_POINT;
  w->thousands        = THOUSAND_SEP;
  w->delm             = JLIB2_CHAR_NULL;
  w->col              = -1;

}  /* init_w() */

/*
 * get_delm() -- translate a number or string into a delimiter
 */
char get_delm(FILE *fp, char *s, char *pname)
{
  int d;
  int c = JLIB2_CHAR_NULL;

  if (strlen(s) == 1)
    {
      if ( ((*s) > 47)  && ((*s) < 58) ) /* 0 -- 9 */
	c = (*s) - 48;
      else
	c = (*s);
    }
  else
    {
      if (j2_is_numr(s) == (int) TRUE)
	{
	  d = atoi(s);
	  if ((d < 256) && (d > 0))
	    c = (char) d;
	  else
	    {
	      fprintf(fp, MSG_ERR_E006, s, SWITCH_CHAR, ARG_DELM);
	      fprintf(fp, MSG_ERR_E000, pname, SWITCH_CHAR, ARG_HELP);
	      exit(EXIT_FAILURE);
	    }
	}
      else
	{
	  fprintf(fp, MSG_ERR_E006, s, SWITCH_CHAR, ARG_DELM);
	  fprintf(fp, MSG_ERR_E000, pname, SWITCH_CHAR, ARG_HELP);
	  exit(EXIT_FAILURE);
	}
    }

  return(c);

} /* get_delm() */

/*
 * save_fname() -- Check and Save File Name
 */
void save_fname(struct s_file_info *f, char *afname, char *pname, char arg_val)
{
  if (f->fname == (char *) NULL)
    f->fname = strdup(afname);
  else
    {
      fprintf(stderr, MSG_ERR_E074, SWITCH_CHAR, arg_val);
      fprintf(stderr, MSG_ERR_E000, pname, SWITCH_CHAR, ARG_HELP);
      exit(EXIT_FAILURE);
    }

} /* save_fname() */

/*
 * process_arg() -- process arguments
 */
int process_arg(int argc, char **argv, struct s_work *w)

{
  char ckarg[SCKARG];
  int opt;
  int i;
  long int li = 0L;
  int col_specified = FALSE;

  snprintf(ckarg, SCKARG, "%c%c%c%c%c:%c:%c:%c:%c:", 
	  ARG_FORCE, ARG_HELP, ARG_VERBOSE, ARG_VERSION,
	  ARG_COL, ARG_DECIMAL, ARG_DELM, ARG_ERR, ARG_OUT);

  while ((opt = getopt(argc, argv, ckarg)) != -1)
    {
      switch (opt)
	{
	case ARG_COL:
	  if (j2_is_numr(optarg) == TRUE)
	    {
	      li = atol(optarg) - 1;
	      if (li < 0)
		{
		  fprintf(stderr, MSG_ERR_E006, optarg, SWITCH_CHAR, ARG_COL);
		  fprintf(stderr, MSG_ERR_E000, w->prog_name, SWITCH_CHAR, ARG_HELP);;
		  exit(EXIT_FAILURE);
		}
	      w->col = atol(optarg) - 1;
	      col_specified = TRUE;
	    }
	  else
	    {
	      fprintf(stderr, MSG_ERR_E006, optarg, SWITCH_CHAR, ARG_COL);
	      fprintf(stderr, MSG_ERR_E000, w->prog_name, SWITCH_CHAR, ARG_HELP);;
	      exit(EXIT_FAILURE);
	    }
	  break;
	case ARG_DECIMAL:
	  w->decimal = get_delm(stderr, optarg, LIT_DECIMAL_P);
	  if ((w->decimal != ',') && (w->decimal != '.') )
	    {
	      fprintf(stderr, MSG_ERR_E072, SWITCH_CHAR, ARG_DECIMAL);
	      fprintf(stderr, MSG_ERR_E000, w->prog_name, SWITCH_CHAR, ARG_HELP);;
	      exit(EXIT_FAILURE);
	    }
	  break;
	case ARG_DELM:
	  w->delm = get_delm(stderr, optarg, w->prog_name);
	  break;
	case ARG_FORCE:
	  w->force = (int) TRUE;
	  break;
	case ARG_HELP:
	  show_brief_help(stderr, w->prog_name);
	  break;
	case ARG_VERBOSE:
	  w->verbose++;
	  break;
	case ARG_VERSION:
	  show_rev(stderr, w->prog_name);
	  break;
	case ARG_ERR:
	  save_fname(&(w->err), optarg, w->prog_name, ARG_ERR);
	  break;
	case ARG_OUT:
	  save_fname(&(w->out), optarg, w->prog_name, ARG_ERR);
	  break;
	default:
	  fprintf(stderr, MSG_ERR_E000, w->prog_name, SWITCH_CHAR, ARG_HELP);
	  exit(EXIT_FAILURE);
	  break;
	}
    }

  /*** open 'out' files ***/
  if ( ! open_out(stderr, &(w->err), w->prog_name, w->force))
    w->err.fp = stderr;
  if ( ! open_out(w->err.fp, &(w->out), w->prog_name, w->force) )
    w->out.fp = stdout;

  /*** Count number of files to process */
  for (i = optind; i < argc; i++)
    (w->num_files)++;
  if (w->num_files == 0)
    (w->num_files)++;  /* stdin when no files */

  return(col_specified);

} /* process_arg() */

/*
 * init() -- initialize
 */
void init(int argc, char **argv, struct s_work *w)

{

  int col_specified = FALSE;

  init_w(w, argv[0]);

  col_specified = process_arg(argc, argv, w);

  switch (w->decimal)
    {
    case '.':
      w->thousands = ',';
      break;
    case ',':
      w->thousands = '.';
      break;
    default:
      fprintf(w->err.fp, MSG_ERR_E101, w->decimal, '.', ',');
      fprintf(w->err.fp, MSG_ERR_E000, w->prog_name, SWITCH_CHAR, ARG_HELP);
      exit(EXIT_FAILURE);
      break;
    }

  if ((w->delm != JLIB2_CHAR_NULL) && (col_specified == FALSE))
    {
      fprintf(w->err.fp, MSG_ERR_E097, SWITCH_CHAR, ARG_COL, SWITCH_CHAR, ARG_DELM);
      fprintf(w->err.fp, MSG_ERR_E000, w->prog_name, SWITCH_CHAR, ARG_HELP);
      exit(EXIT_FAILURE);
    }
  if (col_specified == FALSE) /* set default */
    w->col = 0;

  if (w->verbose > 1) /* show arguments */
    {
      fprintf(w->err.fp, "%s:\n", LIT_COMMAND_LINE_ARGUMENTS);
      fprintf(w->err.fp, "    %-25s %ld\n", LIT_COLUMN_USED, w->col + 1);
      if (w->delm != JLIB2_CHAR_NULL)
	fprintf(w->err.fp, "    %-25s %c\n", LIT_FIELD_DELIMITER, w->delm);
      fprintf(w->err.fp, "    %-25s %c\n", LIT_INPUT_DECIMAL_POINT, w->decimal);
      fprintf(w->err.fp, "    %-25s %d\n", LIT_VERBOSE_LEVEL, w->verbose);
      fprintf(w->err.fp, "    %-25s %s\n", LIT_OVERWRITE_FILES,
	      (w->force == TRUE ? LIT_YES : LIT_NO));
      fprintf(w->err.fp, "    %-25s ", LIT_ERROR_FILE);
      print_fname(w->err.fp, w->err.fname, LIT_STDERR, FILE_NAME_STDERR);
      fprintf(w->err.fp, "\n");
      fprintf(w->err.fp, "    %-25s ", LIT_OUTPUT_FILE);
      print_fname(w->err.fp, w->out.fname, LIT_STDOUT, FILE_NAME_STDOUT);
      fprintf(w->err.fp, "\n");
    }
  
}  /* init() */
