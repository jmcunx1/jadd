## jadd - Add a list of numbers

jadd Just add a list of numbers and prints the sum,
minimum, maximum and average from the list.

This was created for 2 reasons:
* Add a very large list of Numbers
* Systems without bc(1) or dc(1)
* on some systems, bc(1) would overflow depending
  on the number of Numbers to add or the size
  of the Number.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jadd) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jadd.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jadd.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
