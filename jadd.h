/*
 * Copyright (c) 2023 ... 2025 2026
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software
 * for any purpose with or without fee is hereby granted,
 * provided that the above copyright notice and this permission
 * notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef JADD_H

#define PROG_NAME  "jadd"
#define JADD_H "1.10 2025/03/01"

#define NUMBER long double

#ifndef JLIB2_CHAR_NULL
#define NO_JLIB 1
#define JLIB2_INT_NULL   ((int) '\0')
#define JLIB2_CHAR_NULL  ((char) '\0')
#define JLIB2_UCHAR_NULL ((unsigned char) '\0' )
#endif

#ifndef NULL
#define NULL '\0'
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#ifndef SSIZE_T
#define SSIZE_T ssize_t
#endif
#ifndef TAB_CHAR
#define TAB_CHAR 0x09
#endif
#ifndef DECIMAL_POINT
#define DECIMAL_POINT    '.'
#endif
#ifndef THOUSAND_SEP
#define THOUSAND_SEP     ','
#endif

/*** structures ***/
struct s_values
{
  size_t file_count;
  size_t reads;
  size_t writes;
  NUMBER current_value;
  NUMBER min;
  NUMBER max;
  NUMBER average;
  NUMBER total;
  char *raw_value;
} ;

struct s_file_info
{
  FILE *fp;
  char *fname;
} ;

struct s_work
{
  struct s_file_info out;         /* default stdout        */
  struct s_file_info err;         /* default stderr        */
  char *prog_name;                /* real program name     */
  int num_files;                  /* # of files to process */
  int verbose;                    /* TRUE or FALSE         */
  int force;                      /* TRUE or FALSE         */
  size_t col;                     /* column or positition  */
  char decimal;                   /* decimal point         */
  char thousands;                 /* thousands separator   */
  char delm;                      /* field delimiter       */
} ;


/* first step in removing info messages */
#define LIT_ALL                     "All"
#define LIT_AVERAGE                 "Average"
#define LIT_COLUMN_USED             "Column Used"
#define LIT_COMMAND_LINE_ARGUMENTS  "Command Line Arguments"
#define LIT_INPUT_DECIMAL_POINT     "Input Decimal Point"
#define LIT_ERROR_FILE              "Error File"
#define LIT_FILE                    "File"
#define LIT_FIELD_DELIMITER         "Field Delimiter"
#define LIT_MAXIMUM                 "Maximum"
#define LIT_MINIMUM                 "Minimum"
#define LIT_OUTPUT_FILE             "Output File"
#define LIT_OVERWRITE_FILES         "Overwrite Files if Found"
#define LIT_READS                   "Reads"
#define LIT_TOTAL                   "Total"
#define LIT_VERBOSE_LEVEL           "Verbose Level"
#define LIT_COUNT_FILES             "Count Files"
#define LIT_TOTAL_ALL_FILES         "Total All Files"
#define LIT_PROCESSING              "Processing"
#define LIT_INVALID_NUMERIC         "Invalid Numeric"
#define LIT_INVALID_RECORD          "Invalid Record"

/*** messages ***/
#ifdef NO_JLIB
#define ARG_COL           'C'  /* Column to start with               */
#define ARG_DECIMAL       'D'  /* Decimal Mode                       */
#define ARG_DELM          'd'  /* Field Delimiter                    */
#define ARG_ERR           'e'  /* Output Error File                  */
#define ARG_FORCE         'f'  /* force create files                 */
#define ARG_HELP          'h'  /* Show Help                          */
#define ARG_OUT           'o'  /* Output File                        */
#define ARG_VERBOSE       'v'  /* Verbose                            */
#define ARG_VERSION       'V'  /* Show Version Information           */
#define FILE_NAME_STDERR  "-"
#define FILE_NAME_STDIN   "-"
#define FILE_NAME_STDOUT  "-"
#define LIT_NO            "No"
#define LIT_YES           "Yes"
#define LIT_ERRORS        "Errors...."
#define LIT_RECORD        "Record"
#define LIT_VALUE         "Value"
#define LIT_C80           "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
#define LIT_ULINE_07      "-------"
#define LIT_ULINE_15      "---------------"
#define LIT_ULINE_25      "-------------------------"
#define LIT_ULINE_28      "----------------------------"
#define LIT_ULINE_30      "------------------------------"
#define LIT_DECIMAL_P     "decimal point"
#define LIT_INFO_04       "Build: %s %s\n"
#define LIT_REV           "Revision"
#define LIT_STDIN         "(standard input)"
#define LIT_STDOUT        "(standard output)"
#define LIT_STDERR        "(standard error)"
#define MSG_ERR_E000      "Try '%s %c%c' for more information\n"
#define MSG_ERR_E002      "ERROR E002: Cannot open '%s' for write, processing aborted\n"
#define MSG_ERR_E006      "ERROR E006: '%s' is an invalid value for %c%c\n"
#define MSG_ERR_E025      "ERROR E025: File %s cannot be created, already exists\n"
#define MSG_ERR_E072      "ERROR E072: %c%c invalid, can only specify a period or a comma\n"
#define MSG_ERR_E074      "ERROR E074: 'Too many Arguments specified for %c%c\n"
#define MSG_ERR_E080      "ERROR E080: cannot allocate initial memory : %s\n"
#define MSG_ERR_E097      "ERROR E097: Argument '%c%c' is Required when using Option '%c%c'\n"
#define MSG_ERR_E100      "ERROR E100: pledge() %d: %s\n"
#define MSG_ERR_E101      "ERROR E101: %c is ann invalid decimal point, need '%c' or '%c'\n"
#define MSG_INFO_I023     "I023:            Lines Read:  %9ld\n"
#define MSG_INFO_I035     "I035:         Lines Written:  %9ld\n"
#define MSG_INFO_I072     "I072:    Lines Reads:  %9ld - File %s\n"
#define MSG_INFO_I080     "I080:   Lines Writes:  %9ld - File %s\n"
#define MSG_INFO_I096     "I096:     Read Bytes:  %9ld - File %s\n"
#define MSG_INFO_I097     "I097:    Write Bytes:  %9ld - File %s\n"
#define MSG_INFO_I152S    "I152: Run Time: %f ms\n"
#define MSG_WARN_W002     "W002: Open Error Bypass File '%s' : %s\n"
#define MSG_WARN_W033     "W033: Invalid Numeric Value found on Record %ld, Field/Column %d\n"
#define MSG_WARN_W033L    "W033: Invalid Numeric Value found on Record %ld, Field/Column %ld\n"
#define SWITCH_CHAR       '-'
#define USG_MSG_ARG_COL1         "\t%c%c col\t\t: Numeric Value is in column 'col' or at Position 'col'\n"
#define USG_MSG_ARG_DECIMAL_P    "\t%c%c c\t\t: Decimal Point for Numbers, actual character OR\n"
#define USG_MSG_ARG_DECIMAL_P1   "\t\t\t  Ex: 46 = '.' or 44 = ',' or just '.' or ','\n"
#define USG_MSG_ARG_DELM         "\t%c%c c\t\t: Field delimiter, the actual character OR\n"
#define USG_MSG_ARG_DELM_A       "\t\t\t  the decimal # representing the character.\n"
#define USG_MSG_ARG_DELM_C       "\t\t\t  the decimal # representing the character.\n"
#define USG_MSG_ARG_ERR          "\t%c%c file\t\t: Write errors to file 'file', default stderr\n"
#define USG_MSG_ARG_FORCE        "\t%c%c\t\t: force create of files when found\n"
#define USG_MSG_ARG_HELP         "\t%c%c\t\t: Show brief help and exit\n"
#define USG_MSG_ARG_OUT          "\t%c%c file\t\t: Write output to file 'file', default stdout\n"
#define USG_MSG_ARG_VERBOSE_8    "\t%c%c\t\t: verbose level, each time specified level increases\n"
#define USG_MSG_ARG_VERSION      "\t%c%c\t\t: Show revision information and exit\n"
#define USG_MSG_OPTIONS          "Options\n"
#define USG_MSG_USAGE            "usage:\t%s [OPTIONS] [FILES ...]\n"

struct s_j2_datetime
{
  int month;    /* Month,           01 --12             */
  int dd;       /* Day of Month,    01 -- [28|29|30|31] */
  int yy;       /* two digit year,  00 -- 99            */
  int yyyy;     /* four digit year, 0000 -- 9999        */
  int hh;       /* Hour of the day, 00 -- 23            */
  int minutes;  /* minutes,         00 -- 59            */
  int ss;       /* seconds,         00 -- 59            */
  int mil;      /* milliseconds,    000 -- 999          */
  int tm_isdst; /* Daylight ?  0 = no, > 0 = yes        */
} ;

#endif /* NO_JLIB */

/*** prototypes ***/
void init(int, char **, struct s_work *);
void init_finfo(struct s_file_info *);
void init_vals(struct s_values *v);
void show_brief_help(FILE *, char *);
void show_rev(FILE *, char *);
int  open_out(FILE *, struct s_file_info *, char *, int);
void close_out(struct s_file_info *);
int  open_in(FILE **, char *, FILE *);
void close_in(FILE **, char *);
int fmt_date_time(char *datetime, size_t len);
void print_fname(FILE *fp, char *fname, char *stdname, char *title);

#ifdef NO_JLIB
int j2_f_exist(char *file_name);
char *j2_get_prgname(char *argv_0, char *default_name);
int j2_is_numr(char *s);
long int j2_justleft(char *s);
long int j2_rtw(char *buffer);
long int j2_clr_str(char *s, char c, int size);
SSIZE_T j2_getline(char **buf, size_t *n, FILE *fp);
int j2_bye_char(char *buffer, char c);
int j2_fix_numr(char *buffer, char thousand, char decimal_point);
char *j2_d_fmtdt(char *datetime, size_t len);
void j2_today(struct s_j2_datetime *);
long int j2_fix_delm(char delm, char **fixed, char *buf);
int j2_expand_tab(int tab_size, size_t *out_buf_size, char **out_buf, char *in_buf);
long int j2_chg_char(char old, char new, char *s, SSIZE_T force_size);
#endif /* NO_JLIB */

#endif /* JADD_H*/
